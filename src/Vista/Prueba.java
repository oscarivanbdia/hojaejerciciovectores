/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Util.*;
import java.util.Scanner;

/**
 *
 * @author RYZEN
 */
public class Prueba {
    public static void main(String[] args) {
        
          try {
            System.out.println("Por favor digite el tamaño del vector:");
            Scanner lector = new Scanner(System.in);
            
            int n=lector.nextInt();
            
            System.out.println("Por favor digite limiInicial de elementos aleatorios:");
            int ini=lector.nextInt();
            System.out.println("Por favor digite limifinal de elementos aleatorios:");
            int fin=lector.nextInt();
            
            Vector myVector=new Vector(n);
            myVector.llenarVector(ini, fin);
            System.out.println("\n Vector inicial aleatorio: ");
            System.out.println(myVector.toString());
            System.out.println("\n Elemento a añadir al final:");
            int x1=lector.nextInt();
            myVector.addElementoFin(x1);
            System.out.println("\n Elemento adicionado al final: ");
            System.out.println(myVector.toString());
            System.out.println("\n Vector con elemento a añadir al inicio:");
            int x2=lector.nextInt();
            myVector.addElementoInicio(x2);
            System.out.println("\n Vector con elemento adicionado al inicio: ");
            System.out.println(myVector.toString());
            myVector.sort();
            System.out.println("\n Vector ordenado: \n");
            System.out.println(myVector.toString());
            System.out.println("\n Elemento a añadir con el vector ordenado:");
            int x=lector.nextInt();
            myVector.addElementoOrdenado(x);
            System.out.println(myVector.toString());
            
            
        } catch(java.util.InputMismatchException ex2)
        {
            System.err.println("Error en la entrada de datos enteros:"+ex2.getMessage());
        }
            catch (Exception ex) 
            {
            System.err.println("Error:"+ex.getMessage());
            }
        
    }
}
