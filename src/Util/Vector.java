/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 *
 * @author RYZEN
 */
public class Vector {
    private int vector [];
    
    public Vector(int n) throws Exception {
        if(n<=0)
            throw new Exception("No se puede crear el vector:");

        this.vector=new int[n];
    }
    public void llenarVector(int ini, int fin) throws Exception
    {
        if(ini>=fin)
            throw new Exception("No se pueden crear elementos aleatorios");
    
        for(int i=0;i<this.vector.length;i++)
        {
        
        this.vector[i]=(int) Math.floor(Math.random()*(ini-fin+1)+fin);
        
        }
    }
    
    public void addElementoFin(int x){
        int aux [];
        //redimensionar el vector
        aux = new int[vector.length+1];
        for (int i=0;i<aux.length;i++){
            if(i<vector.length)
                aux[i] = this.vector[i];
            else
                aux[i] = x;
        }
        this.vector = aux;
    }
    
    public void addElementoInicio(int x){
        int aux [];
        //redimensionar el vector
        aux = new int[this.vector.length+1];
        aux[0] = x;
        for (int i=0;i<this.vector.length;i++){
            
            aux[i+1] = this.vector[i];
            
        }
        this.vector = aux;
    }
    
    public void sort() {
        int p, j;
        int aux;
        for (p = 1; p < this.vector.length; p++){ // desde el segundo elemento hasta
            aux = this.vector[p];          // el final, guardamos el elemento y
            j = p - 1;                    // empezamos a comprobar con el anterior
            while ((j >= 0) && (aux < this.vector[j])){ // mientras queden posiciones y el
                this.vector[j + 1] = this.vector[j];  // valor de aux sea menor que los
                j--;                   // de la izquierda, se desplaza a
            }                             // la derecha
            this.vector[j + 1] = aux; // colocamos aux en su sitio
        }
    
    }
    
    public void addElementoOrdenado(int x) {
        this.addElementoFin(x);
        int p, j;
        p=vector.length-1;
        int aux = this.vector[p]; 
        j = p - 1;   
        while ((j >= 0) && (aux < this.vector[j])){ 
            this.vector[j + 1] = this.vector[j]; 
            j--;
        }
        this.vector[j + 1] = aux;
    }
    
    
    
    @Override
    public String toString() {
        
       if(this.vector==null)
            return ("Vector vacío");
        String msg="";
        for(int dato:this.vector)
            msg+=dato+"\t";
        
        return msg;

        
    }
}